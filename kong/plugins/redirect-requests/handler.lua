local kong = kong

-- Import the base kong plugin
local BasePlugin = require "kong.plugins.base_plugin"

-- Extend our plugin from the base plugin
local RedirectRequestsHandler = BasePlugin:extend()

-- Setting this very early to avoid logging any service-tokens
RedirectRequestsHandler.PRIORITY = 10

-- creates a new instance of the plugin
function RedirectRequestsHandler:new()
    RedirectRequestsHandler.super.new(self, "redirect-requests")
end

-- plugin built-in method to handle response header filtering
function RedirectRequestsHandler:access(conf)
    kong.response.exit(conf.status_code, "Redirected", {
        ["Location"] = conf.redirect_url .. kong.request.get_path_with_query()
    })
end

-- return the plugin class
return RedirectRequestsHandler