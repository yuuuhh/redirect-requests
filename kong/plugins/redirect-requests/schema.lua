local typedefs = require "kong.db.schema.typedefs"

return {
  name = "redirect-request",
  fields = {
    { config = {
        type = "record",
        fields = {
            { 
                redirect_url = {
                    type = "string",
                    required = true,
                },
            },
            {
                status_code = {
                    type = "number",
                    default = 302,
                    required = true,
                },
            }
        },
      },
    },
  },
}