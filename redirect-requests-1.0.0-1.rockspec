package = "redirect-requests"
version = "1.0.0-1"


supported_platforms = {"linux", "macosx"}
source = {
  url = "https://gitlab.com/yuuuhh/redirect-requests",
  tag = "1.0.0-1"
}

description = {
  summary = "Plugin for redirect requests with URI & query",
  homepage = "https://gitlab.com/yuuuhh/redirect-requests",
  license = "MIT"
}

dependencies = {
}

local pluginName = "redirect-requests"
build = {
  type = "builtin",
  modules = {
    ["kong.plugins."..pluginName..".handler"] = "kong/plugins/"..pluginName.."/handler.lua",
    ["kong.plugins."..pluginName..".schema"] = "kong/plugins/"..pluginName.."/schema.lua",
  }
}